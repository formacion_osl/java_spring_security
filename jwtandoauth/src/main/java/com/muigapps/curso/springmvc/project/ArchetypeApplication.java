package com.muigapps.curso.springmvc.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication(exclude = {SecurityFilterAutoConfiguration.class})
public class ArchetypeApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(ArchetypeApplication.class, args);
		

		System.out.println("Contraseña encriptada ==> " + (new BCryptPasswordEncoder()).encode("curso1234"));
	}

}
