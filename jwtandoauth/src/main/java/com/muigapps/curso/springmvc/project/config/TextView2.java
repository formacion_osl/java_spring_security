package com.muigapps.curso.springmvc.project.config;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.AbstractView;

@Component("textView2")
public class TextView2 extends AbstractView{

	
	public TextView2 () {
		super();
		setContentType(MediaType.TEXT_PLAIN_VALUE);
	}
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Object title = model.get("title");
		Object data = model.get("data");
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("ESTE ES EL TEXTVIEW 2");
		
		if(title != null) {
			builder.append(title);
			builder.append("\n");
		}
		
		if(data instanceof Iterable) {
			for(Object obj: (Iterable)data) {
				builder.append(obj.toString());
				builder.append("\n");
			}
		} else {
			builder.append(data.toString());
		}
		
		response.setContentType(getContentType());
		response.getOutputStream().write(builder.toString().getBytes());
		response.getOutputStream().flush();
		
	}

}
