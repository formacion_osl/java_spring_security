package com.muigapps.curso.springmvc.project.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.muigapps.curso.springmvc.project.config.ExceptioHandler;
import com.muigapps.curso.springmvc.project.controller.model.Response;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler{

	private Logger logger = LoggerFactory.getLogger(CustomAccessDeniedHandler.class);
	
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
		logger.error("Error CustomAccessDeniedHandler",accessDeniedException);
		
		if(request != null && request.getContentType() != null && request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
			Response<String> responseMess = new Response<String>(false,400,"KO","Credenciales no validas");
			response.setHeader("Content-type", MediaType.APPLICATION_JSON_VALUE);
			response.getWriter().write(new ObjectMapper().writeValueAsString(responseMess)); 
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		} else {
			response.setHeader("Location", "/error400");
			response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		}
		
		
		
	}

}
