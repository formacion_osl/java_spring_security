package com.muigapps.curso.springmvc.project.config.security;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter{
	
	
	private AuthenticationManager authenticationManager;
	
	private UserCursoRepository userCursoRepository;
	

	public JWTUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager,UserCursoRepository userCursoRepository) {
		super();
		this.authenticationManager = authenticationManager;
		this.setAuthenticationFailureHandler(new CustomAuthenticationFailureHandler());
		this.userCursoRepository = userCursoRepository;
	}


	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		
		Map<String, String> credencialesMap = getUsernamePass(request);
		String username = credencialesMap.get("username");
		String password = credencialesMap.get("password");
	
		if(SessionsInvalid.getInstance().isLocked(username)) {
			response.addHeader("locked", "true");
			response.addHeader("lockedmin", String.valueOf(SessionsInvalid.getInstance().minLocked(username)));
			
			throw new UserLockedException("El usuario esta bloqueado por varios intentos");
			
		} else {
			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		}
		
	}
	 
	
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		if(!(failed instanceof UserLockedException)) { 
			Map<String, String> credencialesMap = getUsernamePass(request);
			String username = credencialesMap.get("username");
			SessionsInvalid.getInstance().increment(username);
			if(SessionsInvalid.getInstance().isLocked(username)) {
				response.addHeader("locked", "true");
				response.addHeader("lockedmin", String.valueOf(SessionsInvalid.getInstance().minLocked(username)));
			}
			
		}
		
		super.unsuccessfulAuthentication(request, response, failed);
	}
	
	private Map<String,String> getUsernamePass(HttpServletRequest request){
		Map<String,String> credencialesMap = new HashMap<String, String>();
		
		String username = "";
		String password = "";
		
		if(request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
			FormLogin credenciales;
			try {
				credenciales = new ObjectMapper().readValue(request.getInputStream(), FormLogin.class);
				username = credenciales.username;
				password =  credenciales.password;
			} catch (StreamReadException e) {
				throw new BadCredentialsException("Error al recoger la información de user y pass", e);
			} catch (DatabindException e) {
				throw new BadCredentialsException("Error al recoger la información de user y pass", e);
			} catch (IOException e) {
				throw new BadCredentialsException("Error al recoger la información de user y pass", e);
			}
			
			
		} else if(request.getContentType().equals(MediaType.APPLICATION_FORM_URLENCODED_VALUE)){
			username = request.getParameter("username");
			password = request.getParameter("password");
		}
		
		credencialesMap.put("username", username);
		credencialesMap.put("password", password);
		
		return credencialesMap;
	}

	
	
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
		
		UserDetailsCustom userDetails = (UserDetailsCustom)authResult.getPrincipal();
		SessionsInvalid.getInstance().clearSessionInvalid(userDetails.getUsername());
		
		String token = createToken(userDetails.getUsername(), userDetails.getForceTowFactor()?false:true, response);
		
		String code =  null;
		
		if(userDetails.getForceTowFactor()) {
			code = getRandomNumberString();
			System.out.println("El código a introducir es: " + code);
			Calendar now = Calendar.getInstance();
			now.add(Calendar.MINUTE, 10);
			userCursoRepository.updateCodeTowFactor(code,now.getTime(), userDetails.getUsername());
			
		}
		
		if(request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
			response.getWriter().write(new ObjectMapper().writeValueAsString(new ReponseLogin(token, userDetails.getUsername(), userDetails.getForceTowFactor()))); 
			
		} else if(request.getContentType().equals(MediaType.APPLICATION_FORM_URLENCODED_VALUE)){
			if(code != null) {
				response.setHeader("Location", "/twofactor/");
			} else {
				SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
				if(savedRequest != null && savedRequest.getRedirectUrl()  != null) {
					response.setHeader("Location", savedRequest.getRedirectUrl());
				} else {
					response.setHeader("Location", "/web/client/list/");
				}
			}
			response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		}
	}
	
	public static String createToken(String username, Boolean twoFactorValid, HttpServletResponse response) throws JsonProcessingException {
		InfoUser infouser = new InfoUser();
		infouser.setUsername(username);
		infouser.setTwoFactorValid(twoFactorValid);
		
		String infouserjson = new ObjectMapper().writeValueAsString(infouser);
		
		String token = Jwts.builder().setIssuedAt(new Date()).setIssuer(infouserjson)
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstant.TOKEN_EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstant.SUPER_SECRET_KEY).compact();
		
		response.addHeader(SecurityConstant.HEADER_AUTHORIZACION_KEY, SecurityConstant.TOKEN_BEARER_PREFIX + " " + token);
		response.addCookie(UtilsSecurity.createCookie(SecurityConstant.TOKEN_EXPIRATION_TIME, token));
		
		return token;
	}

	
	public static class InfoUser {
		private String username;
		private Boolean twoFactorValid;

		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public Boolean getTwoFactorValid() {
			return twoFactorValid;
		}
		public void setTwoFactorValid(Boolean twoFactorValid) {
			this.twoFactorValid = twoFactorValid;
		}
		
	}
	
	public static class FormLogin {
		
		private String username;
		private String password;
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
		
	}
	
	public static class ReponseLogin {
		private String token;
		private String user;
		private Boolean twoFactorValid;
		
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public Boolean getTwoFactorValid() {
			return twoFactorValid;
		}
		public void setTwoFactorValid(Boolean twoFactorValid) {
			this.twoFactorValid = twoFactorValid;
		}
		public ReponseLogin(String token, String user, Boolean twoFactorValid) {
			super();
			this.token = token;
			this.user = user;
			this.twoFactorValid = twoFactorValid;
		}
		public ReponseLogin(String token, String user) {
			super();
			this.token = token;
			this.user = user;
		}
		public ReponseLogin() {
			super();
		}
		
	}
	
	public static String getRandomNumberString() {
	    Random rnd = new Random();
	    int number = rnd.nextInt(999999);

	    return String.format("%06d", number);
	}

}
