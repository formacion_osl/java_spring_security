package com.muigapps.curso.springmvc.project.config.security;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.muigapps.curso.springmvc.project.repository.UserCursoRepository;

@Configuration
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true, securedEnabled = true)
public class SecurityConfig{
	
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	@Autowired
	private UserCursoRepository userCursoRepository;
	@Autowired
	private CustomAccessDeniedHandler customAccessDeniedHandler;
	@Autowired
	private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	
	@Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
		return authenticationConfiguration.getAuthenticationManager();

    }

	@Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
		http.sessionManagement().and()
		.cors().disable().csrf().ignoringAntMatchers("/logout","/api/**").and().authorizeRequests().antMatchers("/static/css/**").permitAll()
		.antMatchers(HttpMethod.OPTIONS).permitAll()
		.antMatchers("/").permitAll()
		.antMatchers("/login").permitAll()
		.antMatchers("/error400").authenticated()
		.antMatchers("/static/css/**").permitAll()
		.anyRequest().authenticated()
		.and().apply(new JWTHttpConfigurer(userCursoRepository))
		.and().exceptionHandling().accessDeniedHandler(customAccessDeniedHandler).and().exceptionHandling().authenticationEntryPoint(customAuthenticationEntryPoint)
		.and().logout().addLogoutHandler(new CustomLogoutHandler()).and().oauth2Login().loginPage("/login").defaultSuccessUrl("/loginwithgoogle");

		return http.build();
		
    }

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		List<String> methods = Collections.unmodifiableList(Arrays.asList(HttpMethod.GET.name(), HttpMethod.POST.name(),HttpMethod.PUT.name(), HttpMethod.DELETE.name(),HttpMethod.OPTIONS.name()));
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowedMethods(methods);
		corsConfiguration.addAllowedOriginPattern(CorsConfiguration.ALL);
		List<String> headers = Collections.unmodifiableList(Arrays.asList(HttpHeaders.ACCEPT, HttpHeaders.CONTENT_TYPE, HttpHeaders.CONTENT_LENGTH,
				HttpHeaders.CONTENT_LANGUAGE, HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, HttpHeaders.ACCEPT_ENCODING, HttpHeaders.ACCEPT_LANGUAGE,
				HttpHeaders.CONTENT_ENCODING, HttpHeaders.CONTENT_LANGUAGE, HttpHeaders.AUTHORIZATION, HttpHeaders.ALLOW, "refreshtoken"));
		corsConfiguration.setExposedHeaders(headers);
		source.registerCorsConfiguration("*", corsConfiguration.applyPermitDefaultValues());
		return source;
	}
	
	

}
