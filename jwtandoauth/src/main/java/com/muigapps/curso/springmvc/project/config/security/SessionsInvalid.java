package com.muigapps.curso.springmvc.project.config.security;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class SessionsInvalid {

	private Map<String,InfoSessionInvalid> sessionsInvalidMap = new HashMap<String,InfoSessionInvalid>();
	
	private static SessionsInvalid instance;
	
	private SessionsInvalid() {}
	
	public static SessionsInvalid getInstance() {
		if(instance == null) {
			instance = new SessionsInvalid();
		}
		
		return instance;
	}
	
	public boolean isLocked(String username) {
		boolean result = false;
		if(sessionsInvalidMap.containsKey(username)) {
			if(sessionsInvalidMap.get(username).getCount() % 3 == 0) {
				Calendar now = Calendar.getInstance();
				if (now.compareTo(sessionsInvalidMap.get(username).getDateFinishLocked()) < 0) {
					result = true;
				}
			}
			
		}
		
		return result;
	}
	
	public void increment(String username) {
		if(sessionsInvalidMap.containsKey(username)) {
			sessionsInvalidMap.get(username).setCount(sessionsInvalidMap.get(username).getCount() + 1);
			if(sessionsInvalidMap.get(username).getCount() % 3 == 0) {
				int multiply = sessionsInvalidMap.get(username).getCount()/3;
				
				Calendar now = Calendar.getInstance();
				now.add(Calendar.MINUTE, 3*multiply);
				sessionsInvalidMap.get(username).setDateFinishLocked(now);
			}
		} else {
			sessionsInvalidMap.put(username, new InfoSessionInvalid(username, 1));
		}
	}
	

	public int minLocked(String username) {
		int min = 0;
		if(isLocked(username)) {
			long miliseconDiff = sessionsInvalidMap.get(username).getDateFinishLocked().getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
			min =  Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(miliseconDiff)).intValue();
		}
		return min;
	}
	
	public void clearSessionInvalid(String username) {
		if(sessionsInvalidMap.containsKey(username)) {
			sessionsInvalidMap.remove(username);
		}
	}
	
	
	public ArrayList<InfoSessionInvalid> getAllSessionInvalid(){
		return new ArrayList<InfoSessionInvalid>((Collection<InfoSessionInvalid>)sessionsInvalidMap.values());
	}
	
	
	
	
	private static class InfoSessionInvalid {
		
		private String username;
		private int count;
		private Calendar dateFinishLocked;
		
		public int getCount() {
			return count;
		}
		public void setCount(int count) {
			this.count = count;
		}
		public Calendar getDateFinishLocked() {
			return dateFinishLocked;
		}
		public void setDateFinishLocked(Calendar dateFinishLocked) {
			this.dateFinishLocked = dateFinishLocked;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public InfoSessionInvalid() {
			super();
		}
		public InfoSessionInvalid(String username, int count) {
			super();
			this.username = username;
			this.count = count;
		}
		
		
		
		
		
	}
}
