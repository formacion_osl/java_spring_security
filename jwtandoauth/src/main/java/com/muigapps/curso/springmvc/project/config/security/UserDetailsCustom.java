package com.muigapps.curso.springmvc.project.config.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class UserDetailsCustom extends User{

	private static final long serialVersionUID = 2410493266724666926L;
	
	private Boolean forceTowFactor;
	
	
	public UserDetailsCustom() {
		super("default", "default", new ArrayList<GrantedAuthority>());
	}

	public UserDetailsCustom(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}
	
	public UserDetailsCustom(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public Boolean getForceTowFactor() {
		return forceTowFactor;
	}

	public void setForceTowFactor(Boolean forceTowFactor) {
		this.forceTowFactor = forceTowFactor;
	}
	
	
	
	

}
