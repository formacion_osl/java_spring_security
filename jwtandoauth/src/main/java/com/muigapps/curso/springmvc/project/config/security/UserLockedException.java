package com.muigapps.curso.springmvc.project.config.security;

import org.springframework.security.core.AuthenticationException;

public class UserLockedException extends AuthenticationException{

	public UserLockedException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 8474584415650128550L;

}
