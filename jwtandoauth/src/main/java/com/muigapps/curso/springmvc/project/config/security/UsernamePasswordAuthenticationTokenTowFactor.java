package com.muigapps.curso.springmvc.project.config.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

public class UsernamePasswordAuthenticationTokenTowFactor extends UsernamePasswordAuthenticationToken{

	private Boolean validTwoFactor;
	
	public UsernamePasswordAuthenticationTokenTowFactor(Object principal, Object credentials,
			Collection<? extends GrantedAuthority> authorities, Boolean validTwofactor) {
		super(principal, credentials, authorities);
		this.validTwoFactor = validTwofactor;
	}

	public Boolean getValidTwoFactor() {
		return validTwoFactor;
	}

	public void setValidTwoFactor(Boolean validTwoFactor) {
		this.validTwoFactor = validTwoFactor;
	}
	
	

}
