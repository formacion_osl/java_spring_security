package com.muigapps.curso.springmvc.project.config.security;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.Cookie;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.muigapps.curso.springmvc.project.model.Permission;
import com.muigapps.curso.springmvc.project.model.Role;
import com.muigapps.curso.springmvc.project.model.UserCurso;

public class UtilsSecurity {

	
	public static Cookie createCookie(Long age, String token) {
		Cookie cookie = new Cookie(SecurityConstant.COOKIE_AUTHORIZACION_KEY, token);
		cookie.setMaxAge(age.intValue());
		
		return cookie;
	}

	public static  String searchTokenInCookies(Cookie [] cookies) {
		String result = null;
		
		if(cookies != null && cookies.length > 0) {
			for (Cookie cookie: cookies) {
				if(cookie.getName().equals(SecurityConstant.COOKIE_AUTHORIZACION_KEY)) {
					result = cookie.getValue();
				}
			}
		}
		
		return result;
	}
	
	public static Set<GrantedAuthority> getPermissions(UserCurso user){
		Set<GrantedAuthority> permissions = new HashSet<>();
		
		if(user.getRoles() != null && !user.getRoles().isEmpty()) {
			for(Role role: user.getRoles()) {
				permissions.add(new SimpleGrantedAuthority("ROLE_"+role.getName()));
				
				if(role.getPermissions() != null && !role.getPermissions().isEmpty()) {
					for(Permission permission: role.getPermissions()) {
						permissions.add(new SimpleGrantedAuthority(permission.getName()));
					}
				}
			}
		}
		
		return permissions;
	}
	
	public static Set<GrantedAuthority> getPermissionsTwofactor(){
		Set<GrantedAuthority> permissions = new HashSet<>();
		permissions.add(new SimpleGrantedAuthority("TWOFACTOR"));
		
		return permissions;
	}
}
