package com.muigapps.curso.springmvc.project.controller.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.controller.model.Response;
import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.ProductManager;
import com.muigapps.curso.springmvc.project.model.Product;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	@Autowired
	private ProductManager productManager;
	

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	@RequestMapping(path = {"/all","/all/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseList<Product> findAll(){
		List<Product> products = productManager.findAll();
		
		return new ResponseList<Product>(true, 200, "ok", products);
	}
	
	@RequestMapping(path = {"/{id}","/{id}/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Response<Product> getOne(@PathVariable("id") Long id) throws CursoNotFoundException{
		Product product = productManager.findOne(id);
		
		return new Response<Product>(true, 200, "ok", product);
	}


	@RequestMapping(path = {"/one","/one/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Response<Product> getOneParam(@RequestParam(name = "id", required = true) Long id) throws CursoNotFoundException{
		Product product = productManager.findOne(id);
		
		return new Response<Product>(true, 200, "ok", product);
	}
	

	@RequestMapping(path = {"/one/request","/one/request"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Response<Product> getOneParam(HttpServletRequest request) throws CursoSpringMVCException, CursoNotFoundException{
		String idAux = request.getParameter("id");
		
		Long id = null;
		
		try {
			id = Long.valueOf(idAux);
		}catch (Exception e) {
			throw new CursoSpringMVCException(400, messageSource.getMessage("client.id.notvalid", null, localResolver.resolveLocale(request)), null);
		}
		
		Product product = productManager.findOne(id);
		
		return new Response<Product>(true, 200, "ok", product);
	}
	
	@RequestMapping(path = {"","/", "/create","/create/"}, method = RequestMethod.POST,produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE} )
	public Response<Product> create(@RequestBody Product product) throws CursoSpringMVCException{
		Product productResult = productManager.save(product);
		
		return new Response<Product>(true,200,"ok", productResult);
	}
	
	@RequestMapping(path = {"/{id}","/{id}/", "/update/{id}","/update/{id}/"}, method = RequestMethod.PUT,produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE} )
	public Response<Product> update(@PathVariable("id") Long id, @RequestBody Product product) throws CursoSpringMVCException{
		Product productResult = productManager.update(id,product);
		
		return new Response<Product>(true,200,"ok", productResult);
	}
	

	@RequestMapping(path = {"/{id}","/{id}/", "/delete/{id}","/delete/{id}/"}, method = RequestMethod.DELETE,produces = {MediaType.APPLICATION_JSON_VALUE} )
	public Response<Boolean> delete(@PathVariable("id") Long id) throws CursoSpringMVCException{
		productManager.delete(id);
		
		return new Response<Boolean>(true,200,"ok", true);
	}
}
