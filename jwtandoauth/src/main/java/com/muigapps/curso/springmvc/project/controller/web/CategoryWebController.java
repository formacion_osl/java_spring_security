package com.muigapps.curso.springmvc.project.controller.web;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.config.annotations.Lectores;
import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.CategoryManager;
import com.muigapps.curso.springmvc.project.model.Category;

@Controller
@RequestMapping("${url.category.base}")
public class CategoryWebController {

	@Autowired
	private CategoryManager manager;
	
	@Value("${url.category.base}")
	private String urlBase;
	@Value("${url.category.edit}")
	private String urlEdit;
	@Value("${url.category.create}")
	private String urlCreate;
	@Value("${url.category.list}")
	private String urlList;
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	
	private final static int SIZE_PAGE = 10;

	@Lectores
	@RequestMapping(path = {"${url.category.list}"}, method = RequestMethod.GET)
	public String getAll(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		return getAllPage(0, model, request, response);
	}
	
	
	@Lectores
	@RequestMapping(path = {"${url.category.list}{page}", "${url.category.list}{page}/"}, method = RequestMethod.GET)
	public String getAllPage(@PathVariable("page")Integer pageNum, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		ResponseList<Category> page = manager.findPage(pageNum, SIZE_PAGE);
		
		model.addAttribute("title",messageSource.getMessage("category.list", null, localResolver.resolveLocale(request)));
		model.addAttribute("categories", page);
		model.addAttribute("menu","category");
		model.addAttribute("fragmentfile", "category/listcategory");
		model.addAttribute("fragmentname","listcategory");
		
		return "layout/layout";
	}
	
	@Lectores
	@RequestMapping(path = {"${url.category.view}{id}", "${url.category.view}{id}/"}, method = RequestMethod.GET)
	public String getOne(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Category category;
		try {
			category = manager.findOne(id);
		} catch (CursoNotFoundException e) {
			category = null;
		}
		
		Object[] args = new Object[1];
		args[0] = (category != null ? category.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("category.view", args, localResolver.resolveLocale(request)));
		model.addAttribute("category", category);
		model.addAttribute("menu","category");
		model.addAttribute("fragmentfile", "category/viewcategory");
		model.addAttribute("fragmentname","viewcategory");
		
		
	
		
		return "layout/layout";
	}
	

	@PreAuthorize("hasAnyRole('WRITER','ADMIN')")
	@RequestMapping(path = {"${url.category.create}"})
	public String create(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		model.addAttribute("title",messageSource.getMessage("category.create", null, localResolver.resolveLocale(request)));
		model.addAttribute("category", new Category());
		model.addAttribute("menu","category");
		model.addAttribute("fragmentfile", "category/categoryform");
		model.addAttribute("fragmentname","categoryform");
		
		
		return "layout/layout";
	}
	

	@PreAuthorize("hasAnyRole('WRITER','ADMIN')")
	@RequestMapping(path = {"${url.category.edit}{id}","${url.category.edit}{id}/"})
	public String edit(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = request.getParameter("save") != null ? Boolean.valueOf(request.getParameter("save")) : null;
		String message = request.getParameter("message") != null ?request.getParameter("message") : null;
		
		Category category = null;
		
		try {
			category = manager.findOne(id);
		}catch (Exception e) {
			// TODO: handle exception
		}

		Object[] args = new Object[1];
		args[0] = (category != null ? category.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("category.edit", args, localResolver.resolveLocale(request)));
		model.addAttribute("category", category);
		model.addAttribute("menu","category");
		model.addAttribute("fragmentfile", "category/categoryform");
		model.addAttribute("fragmentname","categoryform");
		model.addAttribute("save", save);
		model.addAttribute("message",message);
		
		
		return "layout/layout";
	}

	@PreAuthorize("hasAnyRole('WRITER','ADMIN')")
	@RequestMapping(path = {"${url.category.save}"}, method = RequestMethod.POST)
	public String save(Category category, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		Boolean save = false;
		String message = null;
		
		try {
			if(category.getId() == null) {
				category = manager.save(category);	save = true;
				message =  messageSource.getMessage("category.save", null, localResolver.resolveLocale(request));
			} else {
				category = manager.update(category.getId(), category);
				save = true;
				message =  messageSource.getMessage("category.save.edit", null, localResolver.resolveLocale(request));
			}
			
		}catch (CursoSpringMVCException e) {
			save = false;
			message = e.getMessageError();
		}catch (Exception e) {
			save = false;
			message =  messageSource.getMessage("error.save.generic", null, localResolver.resolveLocale(request));
		}
		
		
		if(save) {
			return "redirect:"+urlBase+urlEdit+category.getId()+"?save="+save+"&message="+URLEncoder.encode(message);
		} else {
			
			model.addAttribute("save", save);
			model.addAttribute("message",message);
			if(category.getId() == null) {
	    		model.addAttribute("title",messageSource.getMessage("category.create", null, localResolver.resolveLocale(request)));
	    	} else {
	    		Object[] args = new Object[1];
	    		args[0] = (category != null ? category.getName() : "");
	    		
	    		model.addAttribute("title",messageSource.getMessage("category.edit", args, localResolver.resolveLocale(request)));
	    	}
			model.addAttribute("category", category);
			model.addAttribute("menu","category");
			model.addAttribute("fragmentfile", "category/categoryform");
			model.addAttribute("fragmentname","categoryform");
			
			
			return "layout/layout";
			
		}
	}
	

	@PreAuthorize("hasAnyRole('WRITER','ADMIN')")
	@RequestMapping(path = {"${url.category.delete}{id}","${url.category.delete}{id}/"})
	public String delete(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		try {
		    manager.delete(id);
			return "redirect:"+urlBase+urlList;
		} catch (CursoSpringMVCException e) {
			return "redirect:"+urlBase+urlList+"?error="+URLEncoder.encode(messageSource.getMessage("error.save", null, localResolver.resolveLocale(request)));
		}
		
		
	}
}
