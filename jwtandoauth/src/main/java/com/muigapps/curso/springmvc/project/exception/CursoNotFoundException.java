package com.muigapps.curso.springmvc.project.exception;

public class CursoNotFoundException extends Exception{
	
	private String messageError;

	public String getMessageError() {
		return messageError;
	}

	public void setMessageError(String messageError) {
		this.messageError = messageError;
	}

	public CursoNotFoundException(String messageError) {
		super();
		this.messageError = messageError;
	}
	
	
	public CursoNotFoundException(String messageError, Exception e) {
		super(e);
		this.messageError = messageError;
	}
	

}
