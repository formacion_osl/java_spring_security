package com.muigapps.curso.springmvc.project.manager.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.CategoryManager;
import com.muigapps.curso.springmvc.project.model.Category;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.CategoryRepository;



@Service
public class CategoryManagerImpl extends BaseManagerImpl<Category,Long> implements CategoryManager{

	@Autowired
	private CategoryRepository repository;

	
	
	@Override
	protected BaseEntityRepository<Category,Long> getRepository() {
		return repository;
	}
	
	

	@Transactional
	@Modifying
	@Override
	public Category update(Long id, Category data) throws CursoSpringMVCException {
		if(validateUpdate(id,data)) {
			Category catbd = repository.findById(id).orElse(null);
			if(catbd != null) {
				catbd.setName(data.getName());
				catbd.setImageprin(data.getImageprin());
				catbd.setShortDescription(data.getShortDescription());
				catbd.setShortDescription(data.getShortDescription());
				catbd.setUpdateDate(data.getUpdateDate());
				catbd.setUpdateUser(data.getUpdateUser());
				return getRepository().save(catbd);
			} else {
				return null;
			}
		} else {
			return null;
		}
		
	}
	
}
