package com.muigapps.curso.springmvc.project.manager.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.CategoryManager;
import com.muigapps.curso.springmvc.project.manager.PermissionManager;
import com.muigapps.curso.springmvc.project.model.Category;
import com.muigapps.curso.springmvc.project.model.Permission;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.CategoryRepository;
import com.muigapps.curso.springmvc.project.repository.PermissionRepository;



@Service
public class PermissionManagerImpl extends BaseManagerImpl<Permission,Long> implements PermissionManager{

	@Autowired
	private PermissionRepository repository;

	
	
	@Override
	protected BaseEntityRepository<Permission,Long> getRepository() {
		return repository;
	}
	
	
	
}
