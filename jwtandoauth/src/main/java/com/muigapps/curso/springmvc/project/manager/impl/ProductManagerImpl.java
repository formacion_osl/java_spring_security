package com.muigapps.curso.springmvc.project.manager.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.ProductManager;
import com.muigapps.curso.springmvc.project.model.Product;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.CategoryRepository;
import com.muigapps.curso.springmvc.project.repository.ProductRepository;



@Service
public class ProductManagerImpl extends BaseManagerImpl<Product,Long> implements ProductManager{

	@Autowired
	private ProductRepository repository;
	@Autowired
	private CategoryRepository categoryRepository;
	
	
	@Override
	protected BaseEntityRepository<Product,Long> getRepository() {
		return repository;
	}
	
	
	@Transactional
	@Modifying
	@Override
	public Product save(Product data) throws CursoSpringMVCException {
		validateBeforeSave(data);
		if(data.getCategory() != null && data.getCategory().getId() != null) {
			data.setCategory(categoryRepository.findById(data.getCategory().getId()).orElse(null));
		}
		Product p = getRepository().save(data);
		
		return p;
	}
	
	@Transactional
	@Modifying
	@Override
	public Product update(Long id, Product data) throws CursoSpringMVCException {
		if(validateUpdate(id,data)) {
			Product productbd = repository.findById(id).orElse(null);
			if(productbd == null) {
				return null;
			} else {
				if(data.getCategory() != null && data.getCategory().getId() != null) {
					data.setCategory(categoryRepository.findById(data.getCategory().getId()).orElse(null));
				}
				Product p = getRepository().save(data);
				
				return p;
			}
		} else {
			return null;
		}
		
	}
	

	
}
