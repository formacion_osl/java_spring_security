package com.muigapps.curso.springmvc.project.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.muigapps.curso.springmvc.project.model.Role;

public interface RoleRepository extends BaseEntityRepository<Role, Long> {


	@Query("select r from Role r where lower(r.name) = lower(:name)")
	Role findByName(@Param("name")String name);
}
