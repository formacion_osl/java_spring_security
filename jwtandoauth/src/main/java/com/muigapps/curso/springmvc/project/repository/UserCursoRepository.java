package com.muigapps.curso.springmvc.project.repository;

import java.util.Date;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.muigapps.curso.springmvc.project.model.UserCurso;

public interface UserCursoRepository extends BaseEntityRepository<UserCurso, Long> {

	@Query("select u from UserCurso u where lower(u.username) = lower(:username) or lower(u.email) = lower(:username) and u.enabled = true")
	Optional<UserCurso> findByUsernameOrEmail(String username);
	
	@Transactional
	@Modifying
	@Query("update UserCurso set codeTwoFactor = :code, finishValidCode = :date where lower(username) = lower(:username) or lower(email) = lower(:username)")
	void updateCodeTowFactor(@Param("code") String code,@Param("date") Date date, @Param("username") String username);

}
