package com.muigapps.curso.springmvc.project.config;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class AuditFilter extends OncePerRequestFilter{

	
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String url = request.getRequestURI();
		if(request.getQueryString() != null && !request.getQueryString().isEmpty()) {
			url = url + "?" + request.getQueryString();
		}
		
		int codeResponse = response.getStatus();
		
		SimpleDateFormat format = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
		System.out.println(format.format(new Date()) + " NUEVA PETICION: " + url + " CODIGO DE RESPUESTA: " + codeResponse);
		
		
		response.setHeader("curso", "springmvc2022");
		
		filterChain.doFilter(request, response);
		
		
	}

}
