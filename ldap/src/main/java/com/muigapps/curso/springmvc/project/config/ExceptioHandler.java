package com.muigapps.curso.springmvc.project.config;

import javax.naming.SizeLimitExceededException;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.muigapps.curso.springmvc.project.controller.model.Response;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;

@RestControllerAdvice
public class ExceptioHandler implements ResponseBodyAdvice<Object>{
	
	private Logger logger = LoggerFactory.getLogger(ExceptioHandler.class);
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;


	@ExceptionHandler(value = NoHandlerFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> hanlderErrorNoHandlerFoundException(HttpServletRequest request, HttpServletResponse response, NoHandlerFoundException e){
		
		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage("NoHandlerFoundException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	
	@ExceptionHandler(value = HttpMediaTypeNotAcceptableException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> hanlderErrorHttpMediaTypeNotAcceptableException(HttpServletRequest request, HttpServletResponse response, HttpMediaTypeNotAcceptableException e){
		
		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage("HttpMediaTypeNotAcceptableException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	
	@ExceptionHandler(value = SizeLimitExceededException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> hanlderErrorSizeLimitExceededException(HttpServletRequest request, HttpServletResponse response, SizeLimitExceededException e){
		
		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage("SizeLimitExceededException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	
	
	
	
	

	@ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> hanlderErrorHttpRequestMethodNotSupportedException(HttpServletRequest request, HttpServletResponse response, HttpRequestMethodNotSupportedException e){

		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage("HttpRequestMethodNotSupportedException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	


	@ExceptionHandler(value = MethodNotAllowedException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> hanlderErrorMethodNotAllowedException(HttpServletRequest request, HttpServletResponse response, MethodNotAllowedException e){

		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage("MethodNotAllowedException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	
	
	@ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Response<String> hanlderErrorMethodArgumentTypeMismatchException(HttpServletRequest request, HttpServletResponse response, MethodArgumentTypeMismatchException e){

		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage("MethodArgumentTypeMismatchException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	
	
	@ExceptionHandler(value = CursoSpringMVCException.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public Response<String> hanlderErrorCursoSpringMVCException(HttpServletRequest request, HttpServletResponse response, CursoSpringMVCException e){

		logger.error("Error", e);
		Response<String> result = null;
		try {
			result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage(e.getMessageError(), null, localResolver.resolveLocale(request)) );
		} catch (Exception ex) {
			result = new Response<String>(false, 400, "ERROR HANDLER", e.getMessageError());
		}
		return result;
		
	}
	

	@ExceptionHandler(value = CursoNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> hanlderErrorCursoNotFoundException(HttpServletRequest request, HttpServletResponse response, CursoNotFoundException e){

		logger.error("Error", e);
		
		Response<String> result = null;
		try {
			result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage(e.getMessageError(), null, localResolver.resolveLocale(request)) );
		} catch (Exception ex) {
			result = new Response<String>(false, 400, "ERROR HANDLER", e.getMessageError());
		}
		
		return result;
		
	}
	

	@ExceptionHandler(value = EntityNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public Response<String> hanlderErrorEntityNotFoundException(HttpServletRequest request, HttpServletResponse response, EntityNotFoundException e){

		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER",messageSource.getMessage("EntityNotFoundException", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	
	
	
	

	@ExceptionHandler(value = Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public Response<String> hanlderErrorException(HttpServletRequest request, HttpServletResponse response, Exception e){

		logger.error("Error", e);
		
		Response<String> result = new Response<String>(false, 400, "ERROR HANDLER", messageSource.getMessage("Exception", null, localResolver.resolveLocale(request)));
		
		return result;
		
	}
	
	@ExceptionHandler(value = ConstraintViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Response<String> handleConstraintViolationException(HttpServletRequest request, HttpServletResponse response, ConstraintViolationException e){
		logger.error("NOT FOUND", e);
		
		StringBuilder builder = new StringBuilder();
		for(ConstraintViolation field: e.getConstraintViolations()) {
			try {
				builder.append(messageSource.getMessage(field.getMessage().replace("{", "").replace("}", ""), null, localResolver.resolveLocale(request)));
			}catch (Exception ex) {
				builder.append(field.getMessage());
			}
			builder.append(" | ");
		}
		Response<String> responseMess = new Response<String>(false,400,"KO",builder.toString());
		
		return responseMess;
	}
	
	@ExceptionHandler(value = AuthenticationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public Response<String> handleAuthenticationException(HttpServletRequest request, HttpServletResponse response, AuthenticationException e){
		logger.error("BAD LOGIN", e);
		
		Response<String> responseMess = new Response<String>(false,400,"KO","Credenciales no validas");
		
		return responseMess;
	}
	
	
	
	
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return false;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		return body;
	}

}
