package com.muigapps.curso.springmvc.project.config.security;

import java.io.IOException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.muigapps.curso.springmvc.project.repository.RoleRepository;

import io.jsonwebtoken.Jwts;

public class JWTBasicAuthenticationFilter extends BasicAuthenticationFilter{
	
	
	private RoleRepository roleRepository;
	private SearchUserLdap searchUserLdap;

	public JWTBasicAuthenticationFilter(AuthenticationManager authenticationManager, RoleRepository roleRepository,SearchUserLdap searchUserLdap) {
		super(authenticationManager);
		this.roleRepository = roleRepository;
		this.searchUserLdap = searchUserLdap;
	}
	
	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
		String tokenHeader = req.getHeader(SecurityConstant.HEADER_AUTHORIZACION_KEY);
		String tokenCookie = UtilsSecurity.searchTokenInCookies(req.getCookies());
		
		if ((tokenHeader == null || !tokenHeader.startsWith(SecurityConstant.TOKEN_BEARER_PREFIX)) && tokenCookie == null ) {
			chain.doFilter(req, res);
			return;
		}
		
		UsernamePasswordAuthenticationToken authentication = getAuthentication(tokenHeader == null || tokenHeader.isEmpty() ? SecurityConstant.TOKEN_BEARER_PREFIX+" "+tokenCookie:tokenHeader);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}
	
	private UsernamePasswordAuthenticationToken getAuthentication(String token) throws ServletException {
		String user = Jwts.parser()
				.setSigningKey(SecurityConstant.SUPER_SECRET_KEY)
				.parseClaimsJws(token.replace(SecurityConstant.TOKEN_BEARER_PREFIX, ""))
				.getBody()
				.getSubject();
		
		Map<String,String> map = searchUserLdap.searchUserLdap(user);
		
		if(map == null || map.isEmpty()) {
			throw new ServletException("No se ha encontrado el usuario");
		}
		
		return new UsernamePasswordAuthenticationToken(user, map, UtilsSecurity.getPermissions(map.get("role"),roleRepository));
		
		
	}
	


}
