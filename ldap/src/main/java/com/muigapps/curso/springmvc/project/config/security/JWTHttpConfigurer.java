package com.muigapps.curso.springmvc.project.config.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;

import com.muigapps.curso.springmvc.project.repository.RoleRepository;


public class JWTHttpConfigurer extends AbstractHttpConfigurer<JWTHttpConfigurer, HttpSecurity>{
	
	private RoleRepository roleRepository;
	private SearchUserLdap searchUserLdap;
	
	
	
	public JWTHttpConfigurer(RoleRepository roleRepository,SearchUserLdap searchUserLdap) {
		super();
		this.roleRepository = roleRepository;
		this.searchUserLdap = searchUserLdap;
	}




	@Override
    public void configure(HttpSecurity http) {
        try {

        	final AuthenticationManager authenticationManager = http.getSharedObject(AuthenticationManager.class);
			
        	http.addFilter(new JWTUsernamePasswordAuthenticationFilter(authenticationManager))
			.addFilter(new JWTBasicAuthenticationFilter(authenticationManager,roleRepository,searchUserLdap));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        
        
    }


	

}
