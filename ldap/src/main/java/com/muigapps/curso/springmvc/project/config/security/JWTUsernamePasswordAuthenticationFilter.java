package com.muigapps.curso.springmvc.project.config.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muigapps.curso.springmvc.project.controller.model.Response;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter{
	
	
	private AuthenticationManager authenticationManager;
	

	public JWTUsernamePasswordAuthenticationFilter(AuthenticationManager authenticationManager) {
		super();
		this.authenticationManager = authenticationManager;
		this.setAuthenticationFailureHandler(new CustomAuthenticationFailureHandler());
	}


	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		
		String username = "";
		String password = "";
		
		if(request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
			FormLogin credenciales;
			try {
				credenciales = new ObjectMapper().readValue(request.getInputStream(), FormLogin.class);
				username = credenciales.username;
				password =  credenciales.password;
			} catch (StreamReadException e) {
				throw new BadCredentialsException("Error al recoger la información de user y pass", e);
			} catch (DatabindException e) {
				throw new BadCredentialsException("Error al recoger la información de user y pass", e);
			} catch (IOException e) {
				throw new BadCredentialsException("Error al recoger la información de user y pass", e);
			}
			
			
		} else if(request.getContentType().equals(MediaType.APPLICATION_FORM_URLENCODED_VALUE)){
			username = request.getParameter("username");
			password = request.getParameter("password");
		}
		
		return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		
	}
	
	
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
		
		org.springframework.security.core.userdetails.User username = (org.springframework.security.core.userdetails.User)authResult.getPrincipal();
		
		InfoUser infouser = new InfoUser();
		infouser.setUsername(username.getUsername());
		
		String infouserjson = new ObjectMapper().writeValueAsString(infouser);
		
		String token = Jwts.builder().setIssuedAt(new Date()).setIssuer(infouserjson)
				.setSubject(username.getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstant.TOKEN_EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstant.SUPER_SECRET_KEY).compact();
		
		response.addHeader(SecurityConstant.HEADER_AUTHORIZACION_KEY, SecurityConstant.TOKEN_BEARER_PREFIX + " " + token);
		response.addCookie(UtilsSecurity.createCookie(SecurityConstant.TOKEN_EXPIRATION_TIME, token));
		
		//chain.doFilter(request, response);
		if(request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE)) {
			response.getWriter().write(new ObjectMapper().writeValueAsString(new ReponseLogin(token, username.getUsername()))); 
			
		} else if(request.getContentType().equals(MediaType.APPLICATION_FORM_URLENCODED_VALUE)){
			SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
			if(savedRequest != null && savedRequest.getRedirectUrl()  != null) {
				response.setHeader("Location", savedRequest.getRedirectUrl());
			} else {
				response.setHeader("Location", "/web/client/list/");
			}
			response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
		}
	}
	


	
	public static class InfoUser {
		private String username;

		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
	}
	
	public static class FormLogin {
		
		private String username;
		private String password;
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		
		
	}
	
	public static class ReponseLogin {
		private String token;
		private String user;
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public ReponseLogin(String token, String user) {
			super();
			this.token = token;
			this.user = user;
		}
		public ReponseLogin() {
			super();
		}
		
	}

}
