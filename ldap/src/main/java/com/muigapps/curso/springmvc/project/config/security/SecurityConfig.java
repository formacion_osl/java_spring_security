package com.muigapps.curso.springmvc.project.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.LdapShaPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import com.muigapps.curso.springmvc.project.repository.RoleRepository;

@Configuration
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true, securedEnabled = true)
public class SecurityConfig{
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private SearchUserLdap searchUserLdap;
	@Autowired
	private CustomAccessDeniedHandler customAccessDeniedHandler;
	@Autowired
	private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

	
	@Bean
	public LdapShaPasswordEncoder passwordEncoder() {
		return new LdapShaPasswordEncoder();
	}
	
	
	@Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
		return authenticationConfiguration.getAuthenticationManager();

    }

	@Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.cors().and()
		.csrf().disable().authorizeRequests().antMatchers("/static/css/**").permitAll()
		.antMatchers("/").permitAll()
		.antMatchers("/login").permitAll()
		.antMatchers("/error400").authenticated()
		.antMatchers(HttpMethod.GET,"/web/product/list/").hasAnyRole("PRODUCTO", "ADMIN")
		.antMatchers(HttpMethod.GET,"/web/product/view/**").hasAnyAuthority("VERPRODUCTO", "ROLE_ADMIN").anyRequest().hasAnyRole("ADMIN")
		.and().apply(new JWTHttpConfigurer(roleRepository,searchUserLdap))
		.and().exceptionHandling().accessDeniedHandler(customAccessDeniedHandler).and().exceptionHandling().authenticationEntryPoint(customAuthenticationEntryPoint)
		.and().logout().addLogoutHandler(new CustomLogoutHandler());
		return http.build();
		
    }

	

}
