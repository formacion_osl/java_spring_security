package com.muigapps.curso.springmvc.project.controller.api;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.controller.model.FilterOrderClient;
import com.muigapps.curso.springmvc.project.controller.model.FilterOrderLine;
import com.muigapps.curso.springmvc.project.controller.model.Response;
import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.OrderClientManager;
import com.muigapps.curso.springmvc.project.model.OrderClient;

@RestController
@RequestMapping("/order")
public class OrderController {
	
	@Autowired
	private OrderClientManager orderClientManager;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	@RequestMapping(path = {"/all","/all/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseList<OrderClient> findAll(){
		List<OrderClient> orders = orderClientManager.findAll();
		
		return new ResponseList<OrderClient>(true, 200, "ok", orders);
	}
	
	@RequestMapping(path = {"/{id}","/{id}/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Response<OrderClient> getOne(@PathVariable("id") Long id) throws CursoNotFoundException{
		OrderClient order = orderClientManager.findOne(id);
		
		return new Response<OrderClient>(true, 200, "ok", order);
	}


	@RequestMapping(path = {"/one","/one/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Response<OrderClient> getOneParam(@RequestParam(name = "id", required = true) Long id) throws CursoNotFoundException{
		OrderClient order = orderClientManager.findOne(id);
		
		return new Response<OrderClient>(true, 200, "ok", order);
	}
	

	@RequestMapping(path = {"/one/request","/one/request"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public Response<OrderClient> getOneParam(HttpServletRequest request) throws CursoSpringMVCException, CursoNotFoundException{
		String idAux = request.getParameter("id");
		
		Long id = null;
		
		try {
			id = Long.valueOf(idAux);
		}catch (Exception e) {
			throw new CursoSpringMVCException(400,messageSource.getMessage("client.id.notvalid", null, localResolver.resolveLocale(request)), null);
		}
		
		OrderClient order = orderClientManager.findOne(id);
		
		return new Response<OrderClient>(true, 200, "ok", order);
	}
	
	@RequestMapping(path = {"","/", "/create","/create/"}, method = RequestMethod.POST,produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE} )
	public Response<OrderClient> create(@RequestBody OrderClient order) throws CursoSpringMVCException{
		OrderClient orderResult = orderClientManager.save(order);
		
		return new Response<OrderClient>(true,200,"ok", orderResult);
	}
	
	@RequestMapping(path = {"/{id}","/{id}/", "/update/{id}","/update/{id}/"}, method = RequestMethod.PUT,produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE} )
	public Response<OrderClient> update(@PathVariable("id") Long id, @RequestBody OrderClient order) throws CursoSpringMVCException{
		OrderClient orderResult = orderClientManager.update(id,order);
		
		return new Response<OrderClient>(true,200,"ok", orderResult);
	}
	

	@RequestMapping(path = {"/{id}","/{id}/", "/delete/{id}","/delete/{id}/"}, method = RequestMethod.DELETE,produces = {MediaType.APPLICATION_JSON_VALUE} )
	public Response<Boolean> delete(@PathVariable("id") Long id) throws CursoSpringMVCException{
		orderClientManager.delete(id);
		
		return new Response<Boolean>(true,200,"ok", true);
	}
	
	
	@RequestMapping(path = {"/from/{date}","/from/{date}/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseList<OrderClient> from(@PathVariable("date")@DateTimeFormat(pattern = "ddMMyyyy") Date date){
		FilterOrderClient filter = new FilterOrderClient();
		filter.setFrom(date);
		
		return orderClientManager.findAllFilter(filter);
	}
	
	@RequestMapping(path = {"/from/{date}/to/{to}","/from/{date}/to/{to}/"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseList<OrderClient> fromTo(@PathVariable("date")@DateTimeFormat(pattern = "ddMMyyyy") Date from,@PathVariable("to")@DateTimeFormat(pattern = "ddMMyyyy") Date to ){
		FilterOrderClient filter = new FilterOrderClient();
		filter.setFrom(from);
		filter.setTo(to);
		
		return orderClientManager.findAllFilter(filter);
	}
	
	

	@RequestMapping(path = {"/beteween"}, method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseList<OrderClient> beteween(@RequestParam("from")@DateTimeFormat(pattern = "ddMMyyyy") Date from,@RequestParam("to")@DateTimeFormat(pattern = "ddMMyyyy") Date to ){
		FilterOrderClient filter = new FilterOrderClient();
		filter.setFrom(from);
		filter.setTo(to);
		
		return orderClientManager.findAllFilter(filter);
	}
	
}
