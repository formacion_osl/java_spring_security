package com.muigapps.curso.springmvc.project.repository;

import org.springframework.data.jpa.repository.Query;

import com.muigapps.curso.springmvc.project.model.OrderClient;

public interface OrderClientRepository extends BaseEntityRepository<OrderClient, Long> {


	@Query(value = " select max(cast (coalesce(number_order,'0') as INT)) + 1 from order_client t", nativeQuery = true)
	Integer nextNumber();
	

}
