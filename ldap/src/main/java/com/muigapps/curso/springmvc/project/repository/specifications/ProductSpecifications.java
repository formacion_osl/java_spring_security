package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterProduct;
import com.muigapps.curso.springmvc.project.model.Product;

public class ProductSpecifications implements Specification<Product> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterProduct filter;

	public static ProductSpecifications getIntsance(FilterProduct filter) {
		return new ProductSpecifications(filter);
	}

	public ProductSpecifications(FilterProduct filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		
		predicates.add(root.get("deleteDate").isNull());
		

		if (filter != null) {
			if (filter.getName() != null && !filter.getName().isEmpty()) {
				predicates.add(criteriaBuilder.like(SpecificationsUtils.getExpressionLowerFiled(root.get("name"),criteriaBuilder), SpecificationsUtils.prepareSearchString(filter.getName())));	
			}
			if(filter.getFromPrice() != null) {
				predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), filter.getFromPrice() ));
			}
			if(filter.getToPrice() != null) {
				predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"), filter.getToPrice() ));
			}
			if(filter.getCategory() != null) {
				predicates.add(criteriaBuilder.equal(root.join("category").get("id"), filter.getCategory() ));
			}
			
			
		}
		
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
