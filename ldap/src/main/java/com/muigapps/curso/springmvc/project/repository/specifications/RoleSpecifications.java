package com.muigapps.curso.springmvc.project.repository.specifications;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.muigapps.curso.springmvc.project.controller.model.FilterCategory;
import com.muigapps.curso.springmvc.project.controller.model.FilterPermission;
import com.muigapps.curso.springmvc.project.controller.model.FilterRole;
import com.muigapps.curso.springmvc.project.model.Category;
import com.muigapps.curso.springmvc.project.model.Role;

public class RoleSpecifications implements Specification<Role> {


	private static final long serialVersionUID = -1119531885850044166L;
	
	private FilterRole filter;

	public static RoleSpecifications getIntsance(FilterRole filter) {
		return new RoleSpecifications(filter);
	}

	public RoleSpecifications(FilterRole filter) {
		super();
		this.filter = filter;
	}

	@Override
	public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		
		predicates.add(root.get("deleteDate").isNull());
		
		if (filter != null) {
			if (filter.getName() != null && !filter.getName().isEmpty()) {
				predicates.add(criteriaBuilder.like(SpecificationsUtils.getExpressionLowerFiled(root.get("name"),criteriaBuilder), SpecificationsUtils.prepareSearchString(filter.getName())));	
			}
		}
		
		if (predicates != null && !predicates.isEmpty()) {
			Predicate[] predicatesArray = new Predicate[predicates.size()];
			int pos = 0;
			for (Predicate predicate : predicates) {
				predicatesArray[pos++] = predicate;
			}
			query.distinct(true);
			return criteriaBuilder.and(predicatesArray);
		} else {
			return null;
		}

	}

}
