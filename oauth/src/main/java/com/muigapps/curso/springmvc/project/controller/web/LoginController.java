package com.muigapps.curso.springmvc.project.controller.web;

import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;


@Controller
public class LoginController {
	
	@Value("${url.client.base}")
	private String urlBase;
	@Value("${url.client.list}")
	private String urlList;
	@Value("${url.product.base}")
	private String urlBaseProduct;
	@Value("${url.product.list}")
	private String urlListProduct;
	

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private LocaleResolver localResolver;
	

//	@RequestMapping(path = {"/login"}, method = RequestMethod.GET)
//	public String login(Model model, String error,String locked, HttpServletRequest request, HttpServletResponse response) {
//		
//		if(SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&  !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
//			SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
//			String language = savedRequest != null &&   savedRequest.getParameterMap() != null && savedRequest.getParameterMap().containsKey("language")?savedRequest.getParameterMap().get("language")[0]:null;
//			if(language != null) {
//				return "redirect:"+urlBase+urlList+"?language="+language;
//			} else {
//				return "redirect:"+urlBase+urlList;
//			}
//			
//		} else {
//			SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
//	
//			String language = savedRequest != null &&   savedRequest.getParameterMap() != null && savedRequest.getParameterMap().containsKey("language")?savedRequest.getParameterMap().get("language")[0]:null;
//			Locale en = Locale.ENGLISH;
//			Locale es = new Locale("es", "ES");
//			if(language != null) {
//				localResolver.setLocale(request, response, language.equals("es")?es:en);
//			}
//			if(error != null) {
//				if(locked != null && !locked.isEmpty()) {
//					try {
//						Integer num = Integer.valueOf(locked) + 1;
//						Object [] args = {num};
//						model.addAttribute("error", messageSource.getMessage("login.error.minutes", args, localResolver.resolveLocale(request)));
//					}catch (Exception e) {
//						model.addAttribute("error", messageSource.getMessage("login.error", null, localResolver.resolveLocale(request)));
//					}
//				} else {
//					model.addAttribute("error", messageSource.getMessage("login.error", null, localResolver.resolveLocale(request)));
//				}
//			}
//			
//			
//			return "login";
//		}
//	}
	
	@RequestMapping(path = {"/"}, method = RequestMethod.GET)
	public String home(Model model, String error, String logout) {
		if(SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated() &&  !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
			SimpleGrantedAuthority leerClientes = new SimpleGrantedAuthority("LEER_CLIENTES");
			if(SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(leerClientes)) {
				return "redirect:"+urlBase+urlList;
			} else {
				return "redirect:"+urlBaseProduct+urlListProduct;
			}
		} else {
			return "redirect:/login";
		}
	}
	
	@RequestMapping(path = {"/error400"}, method = RequestMethod.GET)
	public String error400(Model model, HttpServletRequest request) {
		
		model.addAttribute("title", messageSource.getMessage("error.400", null, localResolver.resolveLocale(request)));
		model.addAttribute("menu","client");
		model.addAttribute("fragmentfile", "error/error400");
		model.addAttribute("fragmentname","error400");
		
		return "layout/layout";
	}
	
	
	
	
	
}
