package com.muigapps.curso.springmvc.project.controller.web;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.PermissionManager;
import com.muigapps.curso.springmvc.project.model.Permission;

@Controller
@RequestMapping("${url.permission.base}")
public class PermissionWebController {

	@Autowired
	private PermissionManager manager;
	
	@Value("${url.permission.base}")
	private String urlBase;
	@Value("${url.permission.edit}")
	private String urlEdit;
	@Value("${url.permission.create}")
	private String urlCreate;
	@Value("${url.permission.list}")
	private String urlList;
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	
	private final static int SIZE_PAGE = 10;

	@RequestMapping(path = {"${url.permission.list}"}, method = RequestMethod.GET)
	public String getAll(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		return getAllPage(0, model, request, response);
	}
	
	
	@RequestMapping(path = {"${url.permission.list}{page}", "${url.permission.list}{page}/"}, method = RequestMethod.GET)
	public String getAllPage(@PathVariable("page")Integer pageNum, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		ResponseList<Permission> page = manager.findPage(pageNum, SIZE_PAGE);
		
		model.addAttribute("title",messageSource.getMessage("permission.list", null, localResolver.resolveLocale(request)));
		model.addAttribute("permissions", page);
		model.addAttribute("menu","permission");
		model.addAttribute("fragmentfile", "permission/listpermission");
		model.addAttribute("fragmentname","listpermission");
		
		return "layout/layout";
	}
	

	@RequestMapping(path = {"${url.permission.view}{id}", "${url.permission.view}{id}/"}, method = RequestMethod.GET)
	public String getOne(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Permission permission;
		try {
			permission = manager.findOne(id);
		} catch (CursoNotFoundException e) {
			permission = null;
		}
		
		Object[] args = new Object[1];
		args[0] = (permission != null ? permission.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("permission.view", args, localResolver.resolveLocale(request)));
		model.addAttribute("permission", permission);
		model.addAttribute("menu","permission");
		model.addAttribute("fragmentfile", "permission/viewpermission");
		model.addAttribute("fragmentname","viewpermission");
		
		
	
		
		return "layout/layout";
	}
	
	@RequestMapping(path = {"${url.permission.create}"})
	public String create(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		model.addAttribute("title",messageSource.getMessage("permission.create", null, localResolver.resolveLocale(request)));
		model.addAttribute("permission", new Permission());
		model.addAttribute("menu","permission");
		model.addAttribute("fragmentfile", "permission/permissionform");
		model.addAttribute("fragmentname","permissionform");
		
		
		return "layout/layout";
	}
	
	
	@RequestMapping(path = {"${url.permission.edit}{id}","${url.permission.edit}{id}/"})
	public String edit(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = request.getParameter("save") != null ? Boolean.valueOf(request.getParameter("save")) : null;
		String message = request.getParameter("message") != null ?request.getParameter("message") : null;
		
		Permission permission = null;
		
		try {
			permission = manager.findOne(id);
		}catch (Exception e) {
			// TODO: handle exception
		}

		Object[] args = new Object[1];
		args[0] = (permission != null ? permission.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("permission.edit", args, localResolver.resolveLocale(request)));
		model.addAttribute("permission", permission);
		model.addAttribute("menu","permission");
		model.addAttribute("fragmentfile", "permission/permissionform");
		model.addAttribute("fragmentname","permissionform");
		model.addAttribute("save", save);
		model.addAttribute("message",message);
		
		
		return "layout/layout";
	}
	
	@RequestMapping(path = {"${url.permission.save}"}, method = RequestMethod.POST)
	public String save(Permission permission, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		Boolean save = false;
		String message = null;
		
		try {
			if(permission.getId() == null) {
				permission = manager.save(permission);	save = true;
				message =  messageSource.getMessage("permission.save", null, localResolver.resolveLocale(request));
			} else {
				permission = manager.update(permission.getId(), permission);
				save = true;
				message =  messageSource.getMessage("permission.save.edit", null, localResolver.resolveLocale(request));
			}
			
		}catch (CursoSpringMVCException e) {
			save = false;
			message = e.getMessageError();
		}catch (Exception e) {
			save = false;
			message =  messageSource.getMessage("error.save.generic", null, localResolver.resolveLocale(request));
		}
		
		
		if(save) {
			return "redirect:"+urlBase+urlEdit+permission.getId()+"?save="+save+"&message="+URLEncoder.encode(message);
		} else {
			
			model.addAttribute("save", save);
			model.addAttribute("message",message);
			if(permission.getId() == null) {
	    		model.addAttribute("title",messageSource.getMessage("permission.create", null, localResolver.resolveLocale(request)));
	    	} else {
	    		Object[] args = new Object[1];
	    		args[0] = (permission != null ? permission.getName() : "");
	    		
	    		model.addAttribute("title",messageSource.getMessage("permission.edit", args, localResolver.resolveLocale(request)));
	    	}
			model.addAttribute("permission", permission);
			model.addAttribute("menu","permission");
			model.addAttribute("fragmentfile", "permission/permissionform");
			model.addAttribute("fragmentname","permissionform");
			
			
			return "layout/layout";
			
		}
	}
	
	@RequestMapping(path = {"${url.permission.delete}{id}","${url.permission.delete}{id}/"})
	public String delete(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		try {
		    manager.delete(id);
			return "redirect:"+urlBase+urlList;
		} catch (CursoSpringMVCException e) {
			return "redirect:"+urlBase+urlList+"?error="+URLEncoder.encode(messageSource.getMessage("error.save", null, localResolver.resolveLocale(request)));
		}
		
		
	}
}
