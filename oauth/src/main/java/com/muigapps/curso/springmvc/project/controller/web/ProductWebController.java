package com.muigapps.curso.springmvc.project.controller.web;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.CategoryManager;
import com.muigapps.curso.springmvc.project.manager.ProductManager;
import com.muigapps.curso.springmvc.project.model.Category;
import com.muigapps.curso.springmvc.project.model.Product;

@Controller
@RequestMapping("${url.product.base}")
public class ProductWebController {

	@Autowired
	private ProductManager manager;
	@Autowired
	private CategoryManager categoryManager;
	
	@Value("${url.product.base}")
	private String urlBase;
	@Value("${url.product.edit}")
	private String urlEdit;
	@Value("${url.product.create}")
	private String urlCreate;
	@Value("${url.product.list}")
	private String urlList;

	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	private final static int SIZE_PAGE = 10;

	@RequestMapping(path = {"${url.product.list}"}, method = RequestMethod.GET)
	public String getAll(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		return getAllPage(0, model, request, response);
	}
	

	@RequestMapping(path = {"${url.product.list}{page}", "${url.product.list}{page}/"}, method = RequestMethod.GET)
	public String getAllPage(@PathVariable("page")Integer pageNum, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		ResponseList<Product> page = manager.findPage(pageNum, SIZE_PAGE);

		model.addAttribute("title",messageSource.getMessage("product.list", null, localResolver.resolveLocale(request)));
		model.addAttribute("products", page);
		model.addAttribute("menu","product");
		model.addAttribute("fragmentfile", "product/listproduct");
		model.addAttribute("fragmentname","listproduct");
		
		return "layout/layout";
	}
	

	@RequestMapping(path = {"${url.product.view}{id}", "${url.product.view}{id}/"}, method = RequestMethod.GET)
	public String getOne(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Product product;
		try {
			product = manager.findOne(id);
		} catch (CursoNotFoundException e) {
			product = null;
		}

		Object[] args = new Object[1];
		args[0] = (product != null ? product.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("product.view", args, localResolver.resolveLocale(request)));
		model.addAttribute("product", product);
		model.addAttribute("menu","product");
		model.addAttribute("fragmentfile", "product/viewproduct");
		model.addAttribute("fragmentname","viewproduct");
		
		
		return "layout/layout";
	}
	

	@RequestMapping(path = {"${url.product.create}"})
	public String create(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Product product = new Product();
		product.setCategory(new Category());
		model.addAttribute("title",messageSource.getMessage("product.create", null, localResolver.resolveLocale(request)));
		model.addAttribute("product", product);
		model.addAttribute("menu","product");
		model.addAttribute("fragmentfile", "product/productform");
		model.addAttribute("fragmentname","productform");
		model.addAttribute("categories", categoryManager.findAll());
		
		
		return "layout/layout";
	}
	

	@RequestMapping(path = {"${url.product.edit}{id}","${url.product.edit}{id}/"})
	public String edit(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = request.getParameter("save") != null ? Boolean.valueOf(request.getParameter("save")) : null;
		String message = request.getParameter("message") != null ?request.getParameter("message") : null;
		
		Product product = null;
		
		try {
			product = manager.findOne(id);
			
			if(product != null && product.getCategory() == null) {
				product.setCategory(new Category());
			}
		}catch (Exception e) {
			// TODO: handle exception
		}

		Object[] args = new Object[1];
		args[0] = (product != null ? product.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("product.edit", args, localResolver.resolveLocale(request)));
		model.addAttribute("product", product);
		model.addAttribute("menu","product");
		model.addAttribute("fragmentfile", "product/productform");
		model.addAttribute("fragmentname","productform");
		model.addAttribute("save", save);
		model.addAttribute("message",message);
		model.addAttribute("categories", categoryManager.findAll());
		
		
		return "layout/layout";
	}
	
	/**
	 * 
	 * @param product ==> Tenemos la anotación Valid para comprobar las anotaciones del Objeto Product que las cumpla
	 * @param bindingResult ==> Clase encargada de recoger el resultado, es importante que se encuentre indicado justo despues del atributo con la anotación @Valid
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(path = {"${url.product.save}"}, method = RequestMethod.POST)
	public String save(@Valid Product product,BindingResult bindingResult, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = false;
		String message = null;
		
		if(bindingResult.hasErrors()) {
			message = "";
			for(FieldError error: bindingResult.getFieldErrors()) {
				message = message + error.getDefaultMessage() + " ";
			}
			
			return resolverErrorSave(model, product, message, request);
		}
		
		try {
			if(product.getId() == null) {
				product = manager.save(product);
				save = true;
				message =  messageSource.getMessage("product.save", null, localResolver.resolveLocale(request));
			} else {
				product = manager.update(product.getId(), product);
				save = true;
				message =  messageSource.getMessage("product.save.edit", null, localResolver.resolveLocale(request));
			}
			
			
		}catch (CursoSpringMVCException e) {
			save = false;
			message = e.getMessageError();
		}catch (Exception e) {
			save = false;
			message =  messageSource.getMessage("error.save.generic", null, localResolver.resolveLocale(request));
		}
		
		
		if(save) {
			return "redirect:"+urlBase+urlEdit+product.getId()+"?save="+save+"&message="+URLEncoder.encode(message);
		} else {
			return resolverErrorSave(model, product, message, request);
		}
	}
	
	
	private String resolverErrorSave(Model model, Product product, String message, HttpServletRequest request) {
		model.addAttribute("save", false);
		model.addAttribute("message",message);
		if(product.getId() == null) {
    		model.addAttribute("title",messageSource.getMessage("product.create", null, localResolver.resolveLocale(request)));
    	} else {
    		Object[] args = new Object[1];
    		args[0] = (product != null ? product.getName() : "");
    		
    		model.addAttribute("title",messageSource.getMessage("product.edit", args, localResolver.resolveLocale(request)));
    	}
		model.addAttribute("product", product);
		model.addAttribute("menu","product");
		model.addAttribute("fragmentfile", "product/productform");
		model.addAttribute("fragmentname","productform");
		model.addAttribute("categories", categoryManager.findAll());
		
		
		return "layout/layout";
	}

	@RequestMapping(path = {"${url.product.delete}{id}","${url.product.delete}{id}/"})
	public String delete(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		try {
		    manager.delete(id);
			return "redirect:"+urlBase+urlList;
		} catch (CursoSpringMVCException e) {
			return "redirect:"+urlBase+urlList+"?error="+URLEncoder.encode(messageSource.getMessage("error.save", null, localResolver.resolveLocale(request)));
		}

	}
	
}
