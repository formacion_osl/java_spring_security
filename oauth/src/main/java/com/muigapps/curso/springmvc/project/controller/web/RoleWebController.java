package com.muigapps.curso.springmvc.project.controller.web;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.PermissionManager;
import com.muigapps.curso.springmvc.project.manager.RoleManager;
import com.muigapps.curso.springmvc.project.model.Role;

@Controller
@RequestMapping("${url.role.base}")
public class RoleWebController {

	@Autowired
	private RoleManager manager;
	@Autowired
	private PermissionManager permissionManager;
	
	@Value("${url.role.base}")
	private String urlBase;
	@Value("${url.role.edit}")
	private String urlEdit;
	@Value("${url.role.create}")
	private String urlCreate;
	@Value("${url.role.list}")
	private String urlList;
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	
	private final static int SIZE_PAGE = 10;

	@RequestMapping(path = {"${url.role.list}"}, method = RequestMethod.GET)
	public String getAll(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		return getAllPage(0, model, request, response);
	}
	
	
	@RequestMapping(path = {"${url.role.list}{page}", "${url.role.list}{page}/"}, method = RequestMethod.GET)
	public String getAllPage(@PathVariable("page")Integer pageNum, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		ResponseList<Role> page = manager.findPage(pageNum, SIZE_PAGE);
		
		model.addAttribute("title",messageSource.getMessage("role.list", null, localResolver.resolveLocale(request)));
		model.addAttribute("roles", page);
		model.addAttribute("menu","role");
		model.addAttribute("fragmentfile", "role/listrole");
		model.addAttribute("fragmentname","listrole");
		
		return "layout/layout";
	}
	

	@RequestMapping(path = {"${url.role.view}{id}", "${url.role.view}{id}/"}, method = RequestMethod.GET)
	public String getOne(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Role role;
		try {
			role = manager.findOne(id);
		} catch (CursoNotFoundException e) {
			role = null;
		}
		
		Object[] args = new Object[1];
		args[0] = (role != null ? role.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("role.view", args, localResolver.resolveLocale(request)));
		model.addAttribute("role", role);
		model.addAttribute("menu","role");
		model.addAttribute("fragmentfile", "role/viewrole");
		model.addAttribute("fragmentname","viewrole");
		
		
	
		
		return "layout/layout";
	}
	
	@RequestMapping(path = {"${url.role.create}"})
	public String create(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		model.addAttribute("title",messageSource.getMessage("role.create", null, localResolver.resolveLocale(request)));
		model.addAttribute("role", new Role());
		model.addAttribute("permissions",permissionManager.findAll());
		model.addAttribute("menu","role");
		model.addAttribute("fragmentfile", "role/roleform");
		model.addAttribute("fragmentname","roleform");
		
		
		return "layout/layout";
	}
	
	
	@RequestMapping(path = {"${url.role.edit}{id}","${url.role.edit}{id}/"})
	public String edit(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = request.getParameter("save") != null ? Boolean.valueOf(request.getParameter("save")) : null;
		String message = request.getParameter("message") != null ?request.getParameter("message") : null;
		
		Role role = null;
		
		try {
			role = manager.findOne(id);
		}catch (Exception e) {
			// TODO: handle exception
		}

		Object[] args = new Object[1];
		args[0] = (role != null ? role.getName() : "");
		
		model.addAttribute("title",messageSource.getMessage("role.edit", args, localResolver.resolveLocale(request)));
		model.addAttribute("role", role);
		model.addAttribute("permissions",permissionManager.findAll());
		model.addAttribute("menu","role");
		model.addAttribute("fragmentfile", "role/roleform");
		model.addAttribute("fragmentname","roleform");
		model.addAttribute("save", save);
		model.addAttribute("message",message);
		
		
		return "layout/layout";
	}
	
	@RequestMapping(path = {"${url.role.save}"}, method = RequestMethod.POST)
	public String save(Role role, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		Boolean save = false;
		String message = null;
		
		try {
			if(role.getId() == null) {
				role = manager.save(role);	save = true;
				message =  messageSource.getMessage("role.save", null, localResolver.resolveLocale(request));
			} else {
				role = manager.update(role.getId(), role);
				save = true;
				message =  messageSource.getMessage("role.save.edit", null, localResolver.resolveLocale(request));
			}
			
		}catch (CursoSpringMVCException e) {
			save = false;
			message = e.getMessageError();
		}catch (Exception e) {
			save = false;
			message =  messageSource.getMessage("error.save.generic", null, localResolver.resolveLocale(request));
		}
		
		
		if(save) {
			return "redirect:"+urlBase+urlEdit+role.getId()+"?save="+save+"&message="+URLEncoder.encode(message);
		} else {
			
			model.addAttribute("save", save);
			model.addAttribute("message",message);
			if(role.getId() == null) {
	    		model.addAttribute("title",messageSource.getMessage("role.create", null, localResolver.resolveLocale(request)));
	    	} else {
	    		Object[] args = new Object[1];
	    		args[0] = (role != null ? role.getName() : "");
	    		
	    		model.addAttribute("title",messageSource.getMessage("role.edit", args, localResolver.resolveLocale(request)));
	    	}
			model.addAttribute("role", role);
			model.addAttribute("permissions",permissionManager.findAll());
			model.addAttribute("menu","role");
			model.addAttribute("fragmentfile", "role/roleform");
			model.addAttribute("fragmentname","roleform");
			
			
			return "layout/layout";
			
		}
	}
	
	@RequestMapping(path = {"${url.role.delete}{id}","${url.role.delete}{id}/"})
	public String delete(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		try {
		    manager.delete(id);
			return "redirect:"+urlBase+urlList;
		} catch (CursoSpringMVCException e) {
			return "redirect:"+urlBase+urlList+"?error="+URLEncoder.encode(messageSource.getMessage("error.save", null, localResolver.resolveLocale(request)));
		}
		
		
	}
}
