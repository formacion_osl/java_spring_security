package com.muigapps.curso.springmvc.project.controller.web;

import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;

import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.PermissionManager;
import com.muigapps.curso.springmvc.project.manager.RoleManager;
import com.muigapps.curso.springmvc.project.manager.UserCursoManager;
import com.muigapps.curso.springmvc.project.model.UserCurso;

@Controller
@RequestMapping("${url.usercurso.base}")
public class UserCursoWebController {

	@Autowired
	private UserCursoManager manager;
	@Autowired
	private RoleManager roleManager;
	
	@Value("${url.usercurso.base}")
	private String urlBase;
	@Value("${url.usercurso.edit}")
	private String urlEdit;
	@Value("${url.usercurso.create}")
	private String urlCreate;
	@Value("${url.usercurso.list}")
	private String urlList;
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
	private LocaleResolver localResolver;
	
	
	private final static int SIZE_PAGE = 10;

	@RequestMapping(path = {"${url.usercurso.list}"}, method = RequestMethod.GET)
	public String getAll(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		return getAllPage(0, model, request, response);
	}
	
	
	@RequestMapping(path = {"${url.usercurso.list}{page}", "${url.usercurso.list}{page}/"}, method = RequestMethod.GET)
	public String getAllPage(@PathVariable("page")Integer pageNum, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		ResponseList<UserCurso> page = manager.findPage(pageNum, SIZE_PAGE);
		
		model.addAttribute("title",messageSource.getMessage("usercurso.list", null, localResolver.resolveLocale(request)));
		model.addAttribute("usercursos", page);
		model.addAttribute("menu","usercurso");
		model.addAttribute("fragmentfile", "usercurso/listusercurso");
		model.addAttribute("fragmentname","listusercurso");
		
		return "layout/layout";
	}
	

	@RequestMapping(path = {"${url.usercurso.view}{id}", "${url.usercurso.view}{id}/"}, method = RequestMethod.GET)
	public String getOne(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		UserCurso usercurso;
		try {
			usercurso = manager.findOne(id);
		} catch (CursoNotFoundException e) {
			usercurso = null;
		}
		
		Object[] args = new Object[1];
		args[0] = (usercurso != null ? usercurso.getUsername() : "");
		
		model.addAttribute("title",messageSource.getMessage("usercurso.view", args, localResolver.resolveLocale(request)));
		model.addAttribute("usercurso", usercurso);
		model.addAttribute("menu","usercurso");
		model.addAttribute("fragmentfile", "usercurso/viewusercurso");
		model.addAttribute("fragmentname","viewusercurso");
		
		
	
		
		return "layout/layout";
	}
	
	@RequestMapping(path = {"${url.usercurso.create}"})
	public String create(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		model.addAttribute("title",messageSource.getMessage("usercurso.create", null, localResolver.resolveLocale(request)));
		model.addAttribute("usercurso", new UserCurso());
		model.addAttribute("roles",roleManager.findAll());
		model.addAttribute("menu","usercurso");
		model.addAttribute("fragmentfile", "usercurso/usercursoform");
		model.addAttribute("fragmentname","usercursoform");
		
		
		return "layout/layout";
	}
	
	
	@RequestMapping(path = {"${url.usercurso.edit}{id}","${url.usercurso.edit}{id}/"})
	public String edit(@PathVariable("id")Long id, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		Boolean save = request.getParameter("save") != null ? Boolean.valueOf(request.getParameter("save")) : null;
		String message = request.getParameter("message") != null ?request.getParameter("message") : null;
		
		UserCurso usercurso = null;
		
		try {
			usercurso = manager.findOne(id);
		}catch (Exception e) {
			// TODO: handle exception
		}

		Object[] args = new Object[1];
		args[0] = (usercurso != null ? usercurso.getUsername() : "");
		
		model.addAttribute("title",messageSource.getMessage("usercurso.edit", args, localResolver.resolveLocale(request)));
		model.addAttribute("usercurso", usercurso);
		model.addAttribute("roles",roleManager.findAll());
		model.addAttribute("menu","usercurso");
		model.addAttribute("fragmentfile", "usercurso/usercursoform");
		model.addAttribute("fragmentname","usercursoform");
		model.addAttribute("save", save);
		model.addAttribute("message",message);
		
		
		return "layout/layout";
	}
	
	@RequestMapping(path = {"${url.usercurso.save}"}, method = RequestMethod.POST)
	public String save(UserCurso usercurso, Model model, HttpServletRequest request, HttpServletResponse response) {
		
		
		Boolean save = false;
		String message = null;
		
		try {
			if(usercurso.getId() == null) {
				usercurso = manager.save(usercurso);	save = true;
				message =  messageSource.getMessage("usercurso.save", null, localResolver.resolveLocale(request));
			} else {
				usercurso = manager.update(usercurso.getId(), usercurso);
				save = true;
				message =  messageSource.getMessage("usercurso.save.edit", null, localResolver.resolveLocale(request));
			}
			
		}catch (CursoSpringMVCException e) {
			save = false;
			message = e.getMessageError();
		}catch (Exception e) {
			save = false;
			message =  messageSource.getMessage("error.save.generic", null, localResolver.resolveLocale(request));
		}
		
		
		if(save) {
			return "redirect:"+urlBase+urlEdit+usercurso.getId()+"?save="+save+"&message="+URLEncoder.encode(message);
		} else {
			
			model.addAttribute("save", save);
			model.addAttribute("message",message);
			if(usercurso.getId() == null) {
	    		model.addAttribute("title",messageSource.getMessage("usercurso.create", null, localResolver.resolveLocale(request)));
	    	} else {
	    		Object[] args = new Object[1];
	    		args[0] = (usercurso != null ? usercurso.getUsername() : "");
	    		
	    		model.addAttribute("title",messageSource.getMessage("usercurso.edit", args, localResolver.resolveLocale(request)));
	    	}
			model.addAttribute("usercurso", usercurso);
			model.addAttribute("roles",roleManager.findAll());
			model.addAttribute("menu","usercurso");
			model.addAttribute("fragmentfile", "usercurso/usercursoform");
			model.addAttribute("fragmentname","usercursoform");
			
			
			return "layout/layout";
			
		}
	}
	
	@RequestMapping(path = {"${url.usercurso.delete}{id}","${url.usercurso.delete}{id}/"})
	public String delete(@PathVariable("id") Long id, Model model, HttpServletRequest request) {
		try {
		    manager.delete(id);
			return "redirect:"+urlBase+urlList;
		} catch (CursoSpringMVCException e) {
			return "redirect:"+urlBase+urlList+"?error="+URLEncoder.encode(messageSource.getMessage("error.save", null, localResolver.resolveLocale(request)));
		}
		
		
	}
}
