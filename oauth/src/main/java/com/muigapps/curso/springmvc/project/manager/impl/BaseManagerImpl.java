package com.muigapps.curso.springmvc.project.manager.impl;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;

import com.muigapps.curso.springmvc.project.controller.model.FilterBase;
import com.muigapps.curso.springmvc.project.controller.model.ResponseList;
import com.muigapps.curso.springmvc.project.exception.CursoNotFoundException;
import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.BaseManager;
import com.muigapps.curso.springmvc.project.model.BaseEntity;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.specifications.SpecificationsFactory;


public abstract class BaseManagerImpl<T extends BaseEntity, ID> implements BaseManager<T, ID> {

	public static Integer sizePage = 20;
	
	@Override
	public List<T> findAll() {
		return getRepository().findAll(notDeleted());
	}
	
	@Override
	public ResponseList<T> findPage(Integer page,Integer sizePage) {
		if(sizePage.intValue() > 200) {
			sizePage = 200;
		}
		ResponseList<T> response = new ResponseList<T>();
		response.setSizePage(sizePage);
		response.setPage(page);
		
		PageRequest request = PageRequest.of(page, sizePage);
		Page<T> pageQuery = getRepository().findAll(request);
		response.setData(pageQuery.getContent());
		response.setNumPage(pageQuery.getTotalPages());
		response.setTotal(pageQuery.getTotalElements());
		response.setCode(200);
		
		return response;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public ResponseList<T> findPageFilter(Integer page,Integer sizePage, FilterBase filter) {
		if(sizePage.intValue() > 200) {
			sizePage = 200;
		}
		
		ResponseList<T> response = new ResponseList<T>();
		response.setSizePage(sizePage);
		response.setPage(page);
		PageRequest request = PageRequest.of(page, sizePage, Sort.by(orderDefault(),columnOrderDefault()));
		Page<T> pageQuery = getRepository().findAll(SpecificationsFactory.getSpecification(filter), request);
		response.setData(pageQuery.getContent());
		response.setNumPage(pageQuery.getTotalPages());
		response.setTotal(pageQuery.getTotalElements());
		response.setCode(200);
		
		return response;
	}

	@Transactional
	@Modifying
	@Override
	public T save(T data) throws CursoSpringMVCException {
		validateBeforeSave(data);
		return getRepository().save(data);
	}


	@Transactional
	@Modifying
	@Override
	public T update(ID id, T data) throws CursoSpringMVCException {
		if(validateUpdate(id,data)) {
			return getRepository().save(data);
		} else {
			return null;
		}
		
	}


	@Override
	public T findOne(ID id) throws CursoNotFoundException {
		Optional<T> opt =  getRepository().findById(id);
		if(opt != null && opt.isPresent()) {
			return opt.get();
		} else {
			throw new CursoNotFoundException("generic.error.notfound.id");
		}
	}

	@Override
	public void delete(ID id) throws CursoSpringMVCException {
		T object = getRepository().findById(id).orElse(null);
		if(object == null) {
			throw new CursoSpringMVCException(404, "generic.error.notfound", null);
		} else {
			getRepository().deleteById(id);
		}
	}
	
	protected abstract BaseEntityRepository<T, ID> getRepository();
	
	protected boolean validateUpdate(ID id, T data) throws CursoSpringMVCException {
		return true;
	}
	
	public boolean cacheEnabled() {
		return false;
	}
	
	public String columnOrderDefault() {
		return "id";
	}
	

	public Sort.Direction orderDefault() {
		return Sort.Direction.DESC;
	}
	
	public Specification<T> notDeleted() {


		Specification<T> specificationNotDeleted = (root, query, cb) -> {
			return root.get("deleteDate").isNull();
		};

		return Specification.where(specificationNotDeleted);
	}
	
	public ResponseList<T> findAllFilter(FilterBase filter){
		ResponseList<T> response = new ResponseList<T>();
		List<T> data = getRepository().findAll(SpecificationsFactory.getSpecification(filter));
		response.setData(data);
		response.setNumPage(0);
		response.setTotal(data != null ? Integer.valueOf(data.size()).longValue():0l);
		response.setSizePage(data != null ? data.size():0);
		response.setCode(200);
		
		return response;
	}
	
	protected void validateBeforeSave(T data) throws CursoSpringMVCException {
		
	}

	
}
