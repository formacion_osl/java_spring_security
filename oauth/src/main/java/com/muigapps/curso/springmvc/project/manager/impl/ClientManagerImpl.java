package com.muigapps.curso.springmvc.project.manager.impl;

import java.util.Locale;
import java.util.regex.Pattern;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.muigapps.curso.springmvc.project.exception.CursoSpringMVCException;
import com.muigapps.curso.springmvc.project.manager.ClientManager;
import com.muigapps.curso.springmvc.project.model.Client;
import com.muigapps.curso.springmvc.project.model.Street;
import com.muigapps.curso.springmvc.project.repository.BaseEntityRepository;
import com.muigapps.curso.springmvc.project.repository.ClientRepository;
import com.muigapps.curso.springmvc.project.repository.StreetRepository;

@Service
public class ClientManagerImpl extends BaseManagerImpl<Client, Long> implements ClientManager {

	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private StreetRepository streetRepository;

	@Override
	protected BaseEntityRepository<Client, Long> getRepository() {
		return clientRepository;
	}

	@Transactional
	@Override
	public Client save(Client data) throws CursoSpringMVCException {
		validate(data);
		Street street = data.getStreet();
		if (street != null) {
			data.setStreet(streetRepository.save(data.getStreet()));
		}
		Client result = clientRepository.save(data);
		
		BeanUtils.copyProperties(street, result);

		return result;
	}

	@Override
	public Client update(Long id, Client data) throws CursoSpringMVCException {

		validate(data);
		Client ClientBd = clientRepository.findById(id).orElse(null);
		if (ClientBd != null) {

			Street street = data.getStreet();
			if (street != null) {
				data.setStreet(streetRepository.save(data.getStreet()));
			}

			Client ClientResult = clientRepository.save(data);

			return ClientResult;
		} else {
			return null;
		}
	}
	
	
	private void validate(Client client) throws CursoSpringMVCException {
		 if(client == null) {
			 throw new CursoSpringMVCException(400, "generic.error.notfound", null);
		 }
		 
		 if(client.getName() == null || client.getName().isEmpty()) {
			 throw new CursoSpringMVCException(400, "client.error.name", null);
		 }
		 
		 if(client.getVat() == null || client.getVat().isEmpty()) {
			 throw new CursoSpringMVCException(400, "client.error.cif", null);
		 }
		 
		 if(client.getEmail() == null || client.getEmail().isEmpty()) {
			 throw new CursoSpringMVCException(400, "client.error.email", null);
		 }
		 
		 Pattern pat = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
		 if(!pat.matcher(client.getEmail()).find()) {
			 throw new CursoSpringMVCException(400, "client.error.email.incorrect", null);
		 }
		 
	}
	


}
