package com.muigapps.curso.springmvc.project.repository;

import com.muigapps.curso.springmvc.project.model.Product;
import com.muigapps.curso.springmvc.project.model.Street;

public interface StreetRepository extends BaseEntityRepository<Street, Long> {


}
